import java.lang.NumberFormatException
import java.util.*
val table = arrayOf(charArrayOf('-', '-', '-'),
        charArrayOf('-', '-', '-'),
        charArrayOf('-', '-', '-'))
var Player = 'X'
var turnCount = 0

fun main() {
    Welcome()
    while (true){
        printTable()
        input()
        if (checkWin() || turnCount == 8){
            break
        }
        changePlayer()
    }
    printTable()
    showWinner()

}
private fun input() {
    val read = Scanner(System.`in`)
    while (true) {
        try {
            print(Player + " Input row col : ")
            val row = read.nextInt()-1
            val col = read.nextInt()-1
            if (row > 2 || col >2){
                println("Please input 'row < 3' and 'col < 3'")
                continue
            }else if (row < 0 || col < 0){
                println("Please input 'row > 0' and 'col > 0'")
                continue
            }
            else if (table[row][col] == '-') {
                table[row][col] = Player
            }
            else {
                println("Is not empty!")
                continue
            }
            break

        } catch (e: InputMismatchException) {
            println("Please input row, col in number format !!!!!!")
            if(read.hasNext()){
                read.next()
            }
        }
    }
}

private fun Welcome() {
    println("Welcome to game XO")
}

private fun checkWin(): Boolean {
    if (checkRow(0) || checkRow(1) || checkRow(2)) {
        return true
    }else if (checkCol(0) || checkCol(1) || checkCol(2)) {
        return true
    }else if (checkLeftDiagonal() || checkRightDiagonal()) {
        return true
    }
    return false
}

private fun showWinner() {
    if (turnCount == 8) {
        println("The Both is Draw!!!")
    }else {
        println(Player + " is Winner!!!")
    }
}

private fun checkRow(x: Int): Boolean {
    for (row in table) {
        if (table[x][0].equals(Player) && table[x][1].equals(Player) &&table[x][2].equals(Player))
            return true
    }
    return false
}
private fun checkCol(x: Int): Boolean {
    for (row in table) {
        if (table[0][x].equals(Player) && table[1][x].equals(Player) &&table[2][x].equals(Player))
            return true
    }
    return false
}

private fun checkLeftDiagonal(): Boolean {
    for (row in table) {
        if (table[0][0].equals(Player) && table[1][1].equals(Player) && table[2][2].equals(Player))
            return true
    }
    return false
}

private fun checkRightDiagonal(): Boolean {
    for (row in table) {
        if (table[0][2].equals(Player) && table[1][1].equals(Player) && table[2][0].equals(Player))
            return true
    }
    return false
}

private fun changePlayer() {
    if (Player == 'X') {
        Player = 'O'
    } else {
        Player = 'X'
    }
    turnCount += 1
}
private fun printTable() {
    for (row in table) {
        for (col in row) {
            print(col)
        }
        println()
    }
}