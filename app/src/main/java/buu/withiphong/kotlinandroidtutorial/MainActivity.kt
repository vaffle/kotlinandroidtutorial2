package buu.withiphong.kotlinandroidtutorial

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val btnTextView = findViewById<Button>(R.id.btnTextView)
        btnTextView.setOnClickListener{
            val intent = Intent (MainActivity@this, TextViewActivity::class.java)
            startActivity(intent)
        }
        val btnAutoCompleteTextView = findViewById<Button>(R.id.btnAutoCompleteTextView)
        btnAutoCompleteTextView.setOnClickListener{
            val intent = Intent(MainActivity@this, AutoCompleteTextViewActivity::class.java)
            startActivity(intent)
        }
    }
}